#!/usr/bin/python3

import mysql.connector
from mysql.connector import Error
#import random
import csv

def get_connection():
    try:
        connection = mysql.connector.connect(
            host='db-qcm.cmxn8sf2hsov.eu-north-1.rds.amazonaws.com',  # Remplacez par votre host, typiquement 'localhost'
            database='qcm',  # Remplacez par le nom de votre base de données
            user='admin',  # Remplacez par votre nom d'utilisateur
            password='password'  # Remplacez par votre mot de passe
        )
        if connection.is_connected():
            return connection
    except Error as e:
        print("Erreur lors de la connexion à MySQL", e)
        return None

def get_question(connection):
    requette = "SELECT * FROM qcm_questions"
    cursor = connection.cursor()
    cursor.execute(requette)
    questions = cursor.fetchall()
    return questions


#### Début du programme 

connection = get_connection()
if connection:
    questionnaire = get_question(connection)
    with open('QR.cvs','w') as f:
        writer = csv.writer(f)
        for quest in questionnaire : 
            writer.writerow(quest[1:])
else:
    print("Impossible de se connecter à la base de données.")

