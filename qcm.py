#!/usr/bin/python3

import mysql.connector
from mysql.connector import Error
import random

def get_connection():
    try:
        connection = mysql.connector.connect(
            host='db-qcm.cmxn8sf2hsov.eu-north-1.rds.amazonaws.com',  # Remplacez par votre host, typiquement 'localhost'
            database='qcm',  # Remplacez par le nom de votre base de données
            user='admin',  # Remplacez par votre nom d'utilisateur
            password='password'  # Remplacez par votre mot de passe
        )
        if connection.is_connected():
            return connection
    except Error as e:
        print("Erreur lors de la connexion à MySQL", e)
        return None

def get_random_question(connection):
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM qcm_questions ORDER BY RAND() LIMIT 1")
    question = cursor.fetchone()
    cursor.close()
    return question

def main():
    connection = get_connection()
    if connection:
        question = get_random_question(connection)
        if question:
            print("Question:", question[1])
            print("1:", question[2])
            print("2:", question[3])
            print("3:", question[4])
            print("4:", question[5])
            user_choice = input("Entrez le numéro de la bonne réponse (1-4): ")
            if user_choice.isdigit() and int(user_choice) == question[6]:
                print("Correct!")
            else:
                print("Incorrect. La bonne réponse était:", question[6])
        connection.close()
    else:
        print("Impossible de se connecter à la base de données.")

if __name__ == "__main__":
    main()
