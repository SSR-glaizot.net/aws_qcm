#!/usr/bin/python3

import random
import csv


# lecture du fichier csv
def lire_questions(fichier):
    with open(fichier, newline='', encoding='utf-8') as csvfile:
        lecteur = csv.reader(csvfile)
        questions = list(lecteur)
    return questions


# mélanger le tableau et ne garder que les nb premières questions

def shuffle_questionnaire(questionnaire, nb):
    random.shuffle(questionnaire)
    return questionnaire[:nb]



#### Début du programme 



nbquest=0
correct=0
questionnaire = lire_questions("QR.csv")
questionnaire_final = shuffle_questionnaire(questionnaire, 2)


for question in questionnaire_final : 
    nbquest = nbquest+1
    print(question)
    if question:
        print("Question:", question[0])
        print("1:", question[1])
        print("2:", question[2])
        print("3:", question[3])
        user_choice = input("Entrez le numéro de la bonne réponse (1-4): ")
    
    if user_choice.isdigit() and int(user_choice) == question[4]:
        correct=correct+1
        print("Correct!")
    else:
        print("Incorrect. La bonne réponse était:", question[4])
print("Résultat : ", correct, "sur ", nbquest)


